import React, { useState } from 'react';
import {MainColor} from './style.js';

const Home = props => {
    const products = props.data.map( (product) => {
        return (
            <div className="col-md-4" key={product.id} >
                <div className="card ">
                    <img className="card-img-top" src={product.image} alt={product.title} />
                    <div className="card-body">
                        <h4 className="card-title"> {product.title} </h4>
                        <h6> <MainColor> Price :</MainColor>  {product.price} </h6>
                        <h6> <MainColor> Color :</MainColor>  {product.color} </h6>
                        <h6> <MainColor> Size :</MainColor>  {product.size} </h6>  
                        <button type="button" className="btn btn-primary" onClick={() => addToCart(product)}> AddTo Card </button>
                    </div>
                </div>
            </div> 
        );
    })

    function addToCart(item) {
        var cart =  localStorage.getItem('myCart') ;
        
        if ( cart == null){
            var items = [];
            items.push(item)

            localStorage.setItem('myCart' ,  JSON.stringify(items) ) ;

            console.log("1" ,localStorage.getItem('myCart'))
        }
        else
        {
            var existing =  localStorage.getItem('myCart')  ;
            var r = JSON.parse(existing)
            var x = r.find(x => x.id == item.id);
            if (x == undefined) {
                r.push(item);
                localStorage.setItem('myCart' ,  JSON.stringify(r) ) ; 
            } else {
                alert(" I can't added  ");
            }
               
        }
    }

    function showCart() {
        var i = localStorage.getItem('myCart') ;
        console.log(i);
    }

    function clearCart() {
        localStorage.clear();
        var i = localStorage.getItem('myCart');
        console.log(i);
    }
    
    return (
        <div className="container">
            <div className="row">
                <div className="col-md-12">
                    <div className="text-center">
                        <button onClick={() => showCart()}> show Cart </button>
                        <button onClick={() => clearCart()}> clear Cart </button>
                    </div>
                </div>
                {products}
            </div>
        </div>
    )
}

export default Home;