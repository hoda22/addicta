import React , { useState } from 'react';
import Navbar from '../Navbar'
import Invoice from '../Invoice'

import Footer from '../Footer'
const Cart = () =>{

    var cart = localStorage.getItem('myCart');
    if( !cart ){
        cart = [];
    }
    const [state, setState] = useState({cart});

    return (
        <div> 
            <Navbar /> 
            <Invoice {...state} />
            <Footer />
        </div>
    )
}

export default Cart;