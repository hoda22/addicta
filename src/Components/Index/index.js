import React, { useState, useEffect } from 'react';
import axios from 'axios';
import Navbar from '../Navbar'
import Home from '../Home'
import Footer from '../Footer'
const Index = () =>{
  const [data, setData] = useState({ products: [] });
  useEffect(() => {
    async function fetchData() {
     const result = await axios(
        'js/data.json',
      );
      setData(result.data);
    }
    fetchData();
  }, []);
  return (
    <div> 
        <Navbar />  
        <Home data={data.products} />
        <Footer />
    </div>
  );
}
export default Index;