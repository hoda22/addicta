import React, { useState } from 'react';
import {MainColor} from './style.js';
const Home = props => {
    const [profileState, setProfileState] = useState(props);
    console.log(profileState.cart.length)
    if(profileState.cart.length != 0){
        var productsItems = JSON.parse(profileState.cart)

        var products = productsItems.map( (product) => {
            return (
                <div className="col-md-4" key={product.id} >
                    <div className="card">
                        <img className="card-img-top" src={product.image} alt={product.title} />
                        <div className="card-body">
                            <h4 className="card-title"> {product.title} </h4>
                            <h6> <MainColor> Price :</MainColor>  {product.price} </h6>
                            <h6> <MainColor> Color :</MainColor>  {product.color} </h6>
                            <h6> <MainColor> Size :</MainColor>  {product.size} </h6>  
                        </div>
                    </div>
                </div> 
            );
        })
    }else{
        console.log('tag');
        var products = "No Data"
    }
    
    return (
        <div className="container">
            <div className="row">
                {products}
            </div>
        </div>
    )
}

export default Home;