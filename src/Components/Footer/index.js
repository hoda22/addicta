import React from 'react';

const Footer = () =>{
    return (
        <div className="container text-center">
            <div className="footer">
                <p> copyright &copy; 2018 by Addicta </p>
            </div>
        </div>
    )
}

export default Footer;