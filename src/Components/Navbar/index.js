import React from 'react';
import { Link } from 'react-router-dom';
import {NavbarSection , Logo , UlList ,Anchor ,LiItem ,LogoText } from './style.js';


const Navbar = () =>{
    return (
        <NavbarSection> 
            <div className="container"> 
                <Logo>
                    <LogoText> Addicta </LogoText>
                </Logo>  
                <UlList>
                    <LiItem><Link to="/home"> Home </Link></LiItem>
                    <LiItem><Link to="/cart"> Cart </Link></LiItem>
                </UlList>  
            </div>
        </NavbarSection> 
    )
}

export default Navbar;