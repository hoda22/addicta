import React , { Component } from "react";
import { BrowserRouter ,Route , Switch } from "react-router-dom";

import Index from './Components/Index';
import Cart from './Components/Cart';
import 'bootstrap/dist/css/bootstrap.min.css';

import './App.css';

class App extends Component {
  render(){
    return ( 
      <BrowserRouter>
      <Switch>
        <Route path="/cart" component={Cart} />
        <Route path="/" component={Index} />
      </Switch>
    </BrowserRouter>
    )
  }
}

export default App;